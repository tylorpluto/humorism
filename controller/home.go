package controller

import (
	"net/http"
	"github.com/jinzhu/gorm"
	"time"
	"humorism/model"
	"log"
	"fmt"
	"html/template"
)

type HomeData struct {
	Posts []model.Post
}

func RenderHome(w http.ResponseWriter, r *http.Request, db *gorm.DB) {
	startTime := time.Now()

	posts := make([]model.Post, 0)
	// TODO: Add ordering by Humorism post's own Score logic
	db.Order("created_at desc").Limit(15).Order("reply_count desc").Find(&posts)

	t, err := template.ParseFiles("./template/home.html", "./template/layout.html")
	if err != nil {
		log.Fatal(err)
	}

	hd := HomeData{Posts: posts}

	t.ExecuteTemplate(w, "layout", hd)

	fmt.Println(time.Since(startTime))
}
