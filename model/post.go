package model

import (
	"github.com/jinzhu/gorm"
	"log"
)

type RawPost struct {
	Title      string
	ReplyCount int `gorm:"not null"`
	Link       string
	Source     string
}

type Post struct {
	gorm.Model
	RawPost
	SoftDelete bool `gorm:"NOT NULL;DEFAULT:FALSE"`
	Score      int  `gorm:"DEFAULT:0`
}

func SavePosts(dps *[]RawPost, db *gorm.DB) {
	for _, dp := range *dps {
		oldPost := Post{}
		db.Where("title = ?", dp.Title).First(&oldPost)

		if oldPost.Title == "" {
			post := Post{RawPost: dp}
			if err := db.Create(&post).Error; err != nil {
				db.Rollback()
				log.Fatal(err)
			}
		} else {
			oldPost.ReplyCount = dp.ReplyCount
			if err := db.Save(&oldPost).Error; err != nil {
				db.Rollback()
				log.Fatal(err)
			}
		}
	}
}
