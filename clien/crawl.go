package clien

import (
	"humorism/model"
	"github.com/jinzhu/gorm"
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"strconv"
)

func CrawlPopularPosts(db *gorm.DB) error {
	const MIN_COUNT_TO_CRAWL_MODU_GONGWON = 20
	// 오늘의 추천글
	res, err := http.Get("https://clien.net")
	if err != nil {
		return (err)
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		return (err)
	}

	var posts = make([]model.RawPost, 0)

	doc.Find(".recommended .list_title").Each(func(i int, s *goquery.Selection) {
		title := s.Find(".subject").Text()
		ccText := s.Find(".list_reply span").Text()
		ccInt64, err := strconv.ParseInt(ccText, 10, 64)
		if err != nil {
			return
		}
		cc := int(ccInt64)
		rawLink, _ := s.Find(".list_subject").Attr("href")
		link := "https://clien.net" + rawLink

		post := model.RawPost{Title: title, ReplyCount: cc, Link: link, Source: "clien"}
		posts = append(posts, post)
	})

	// 모두의광장
	res, err = http.Get("https://www.clien.net/service/group/community")
	if err != nil {
		return err
	}
	defer res.Body.Close()

	doc, err = goquery.NewDocumentFromReader(res.Body)
	doc.Find(".list_item:not(.notice)").Each(func(i int, s *goquery.Selection) {
		title := s.Find(".list_title [data-role='list-title-text']").Text()
		rawLink, _ := s.Find("a.list_subject").Attr("href")
		link := "https://clien.net" + rawLink
		ccString := s.Find(".list_reply  span").Text()
		cc := 0
		if len(ccString) > 0 {
			ccInt64, err := strconv.ParseInt(ccString, 10, 64)
			cc = int(ccInt64)
			if err != nil {
				return
			}
		}

		if cc > MIN_COUNT_TO_CRAWL_MODU_GONGWON {
			post := model.RawPost{Title: title, ReplyCount: cc, Link: link, Source: "clien"}
			posts = append(posts, post)
		}
	})

	model.SavePosts(&posts, db)
	return nil
}
