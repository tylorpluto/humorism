package dogdrip

import (
	"net/http"
	"log"
	"io/ioutil"
	"golang.org/x/net/html"
	"strings"
	"strconv"
	"errors"
	"humorism/model"
	"github.com/jinzhu/gorm"
)

func CrawlPopularPosts(db *gorm.DB) error {
	resp, err := http.Get("http://www.dogdrip.net/dogdrip")
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	htmlString := string(body)

	parsedHTML, err := html.Parse(strings.NewReader(htmlString))
	if err != nil {
		return err
	}

	boardNode, err := getBoardNode(parsedHTML)
	if err != nil {
		log.Println(err)
	}

	var dogdripPosts = make([]model.RawPost, 0)
	getPopularPosts(boardNode, &dogdripPosts)
	if len(dogdripPosts) > 0 {
	model.SavePosts(&dogdripPosts, db)
	}
	return nil
}

func getPopularPosts(node *html.Node, dogdripPosts *[]model.RawPost) {
	const MIN_COMMENT_COUNT_TO_CRAWL = 50

	if node.Type == html.ElementNode && node.Data == "span" {
		for _, attr := range node.Attr {
			if attr.Key == "class" && attr.Val == "replyAndTrackback" {
				c, err := strconv.ParseInt(node.LastChild.FirstChild.Data, 10, 64)
				if err != nil {
					return
				}
				cc := int(c)

				rawTitle := node.PrevSibling.PrevSibling.FirstChild.Data
				t := strings.TrimSpace(rawTitle)
				p := model.RawPost{ Title: t, ReplyCount: cc, Link: node.PrevSibling.PrevSibling.Attr[0].Val, Source: "dogdrip"}
				if c >= MIN_COMMENT_COUNT_TO_CRAWL {
					*dogdripPosts = append(*dogdripPosts, p)
				}
			}
		}
	}

	for child := node.FirstChild; child != nil; child = child.NextSibling {
		getPopularPosts(child, dogdripPosts)
	}
}

func getBoardNode(node *html.Node) (*html.Node, error) {
	if node.Type == html.ElementNode && node.Data == "table" {
		for _, attr := range node.Attr {
			if attr.Key == "class" && attr.Val == "boardList" {
				return node, nil
			}
		}

	}

	for child := node.FirstChild; child != nil; child = child.NextSibling {
		nextNode, err := getBoardNode(child)
		if err == nil {
			return nextNode, err
		}
	}

	return nil, errors.New("There isn't any board list to crawl.")
}
