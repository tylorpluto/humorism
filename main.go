package main

import (
	"github.com/jinzhu/gorm"
	//_ "github.com/jinzhu/gorm/dialects/postgres"
	_ "github.com/jinzhu/gorm/dialects/sqlite"
	"log"
	"humorism/model"
	"humorism/dogdrip"
	"humorism/pgr21"
	"humorism/clien"
	"net/http"
	"github.com/robfig/cron"
	"fmt"
	"humorism/controller"
	"os"
	"path/filepath"
)

func main() {
	// Init DB
	//host := os.Getenv("HUMORISM_DB_HOST")
	//port := os.Getenv("HUMORISM_DB_PORT")
	//user := os.Getenv("HUMORISM_DB_USERNAME")
	//dbName := os.Getenv("HUMORISM_DB_NAME")
	//pw := os.Getenv("HUMORISM_DB_PASSWORD")
	//sslmode := os.Getenv("HUMORISM_DB_SSL_MODE")
	//args := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s sslmode=%s", host, port, user, dbName, pw, sslmode)
	//db, err := gorm.Open("postgres", args)
	port := os.Getenv("PORT")

	if port == "" {
		log.Fatal("$PORT must be set")
	}
	path, err := filepath.Abs("./tmp/humorism.db")
	if err != nil {
		log.Fatal(err)
	}
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			os.Mkdir("./tmp", 755)
			f, err := os.Create(path)
			if err != nil {
				log.Fatal(err)
			}
			defer f.Close()
		}
	}
	db, err := gorm.Open("sqlite3", "./tmp/humorism.db")
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Make missing DB tables
	if !db.HasTable(model.Post{}) {
		db.CreateTable(model.Post{})
	}

	// Set Cron JOB to crawl posts
	c := cron.New()
	c.AddFunc("@every 1m", func() { crawlBoards(db) })
	c.Start()

	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) { controller.RenderHome(w, r, db) })
	if err := http.ListenAndServe(":" + port, nil); err != nil {
		panic(err)
	}
}

func crawlBoards(db *gorm.DB) {
	err := clien.CrawlPopularPosts(db)
	if err != nil {
		fmt.Println(err)
	}
	err = dogdrip.CrawlPopularPosts(db)
	if err != nil {
		fmt.Println(err)
	}
	err = pgr21.CrawlPopularPosts(db)
	if err != nil {
		fmt.Println(err)
	}
}
