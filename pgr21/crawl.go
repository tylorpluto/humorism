package pgr21

import (
	"github.com/jinzhu/gorm"
	"net/http"
	"github.com/PuerkitoBio/goquery"
	"humorism/model"
	"strconv"
	"strings"
)

func CrawlPopularPosts(db *gorm.DB) error {
	const MIN_COUNT_TO_CRAWL_FREE_BOARD = 70
	const MIN_COUNT_TO_CRAWL_HUMOR_BOARD = 30

	var posts = make([]model.RawPost, 0)
	// FREE BOARD
	res, err := http.Get("https://pgr21.com/pb/pb.php?id=freedom")
	if err != nil {
		return err
	}
	defer res.Body.Close()

	doc, err := goquery.NewDocumentFromReader(res.Body)
	appendPosts(doc, MIN_COUNT_TO_CRAWL_FREE_BOARD, &posts)

	// FREE BOARD
	res, err = http.Get("https://pgr21.com/pb/pb.php?id=humor")
	if err != nil {
		return err
	}
	defer res.Body.Close()

	doc, err = goquery.NewDocumentFromReader(res.Body)
	appendPosts(doc, MIN_COUNT_TO_CRAWL_HUMOR_BOARD, &posts)

	model.SavePosts(&posts, db)
	return nil
}

func appendPosts(doc *goquery.Document, minCountToCrawl int, posts *[]model.RawPost) *goquery.Selection {
	return doc.Find("td.tdsub:not(.notice)").Each(func(i int, s *goquery.Selection) {
		title := s.Find(".tdsub a").Text()
		rawLink, _ := s.Find(".tdsub a").Attr("href")
		link := "https://pgr21.com/pb/" + rawLink
		ccString := strings.Trim(s.Find(".tdsub span").Text(), "[]")
		cc := 0
		if len(ccString) > 0 {
			ccInt64, err := strconv.ParseInt(ccString, 10, 64)
			cc = int(ccInt64)
			if err != nil {
				return
			}
		}

		if cc > minCountToCrawl {
			post := model.RawPost{Title: title, ReplyCount: cc, Link: link, Source: "pgr21"}
			*posts = append(*posts, post)
		}
	})
}
